** CipherOS 2.5 Lynx **
- Android Security Patch :
 * 5th February 2022

[Misc]
* Bump to CipherOS 5.0 - Antares

[Security]
* This Update Brings Feb 2024 Security Patch & fixes some known issues
* Merged Tag android-14.0.0_r27

[Optimization]
* We've optimized our source even more , reduced debugging & fixed some known bugs.

[System]
* Introducing CipherOS 5.0 based on Android 14

[HomeScreen]
* All-new Launcher!


